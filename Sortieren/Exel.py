# %% 
import numpy as np
import pandas as pd

# %% 
EMISSION_CSV = # Pfad zu co2_emission.csv
GDP_CSV = # Pfad zu gdp.csv
POP_TOTAL_CSV = # Pfad zu pop_toal.csv
POP_GROW_CSV = # Pfad zu POP_GROW.csv
AIR_CSV = # Pfad zu air.csv

# %%
FILTERED_EMISSION_CSV = # Pfad wo bereinigte Datei gespeichtert werden soll
FILTERED_GDP_CSV = # Pfad wo bereinigte Datei gespeichtert werden soll
FILTERED_POP_TOTAL_CSV = # Pfad wo bereinigte Datei gespeichtert werden soll
FILTERED_POP_GROW_CSV = # Pfad wo bereinigte Datei gespeichtert werden soll
FILTERED_AIR_CSV = # Pfad wo bereinigte Datei gespeichtert werden soll


# %% 
emission = np.array(pd.read_csv(filepath_or_buffer = EMISSION_CSV, header=None))
gdp = np.array(pd.read_csv(filepath_or_buffer = GDP_CSV, header=None))
pop_total = np.array(pd.read_csv(filepath_or_buffer = POP_TOTAL_CSV, header=None))
pop_grow = np.array(pd.read_csv(filepath_or_buffer = POP_GROW_CSV, header=None))
air = np.array(pd.read_csv(filepath_or_buffer = AIR_CSV, header=None))

# %%
""" Datenbereinigung: NaN wird entfernt"""
import math
def isnan(array):
    idxs = []
    x = array.shape[0]
    for i in range(x):
        if type(array[i]) == str:
            continue
        if math.isnan(float(array[i])):
            idxs.append(i)
    return idxs

# %%
""" Datenveränderung, population_growth """
pop_grow = pop_grow.T
greater2005 = np.argwhere(pop_grow[4:, 0] >= 2005) + 4
lower2017 = np.argwhere(pop_grow[4:, 0] <= 2017) + 4 
years = np.intersect1d(greater2005, lower2017)
body = pop_grow[years]
header = pop_grow[:4, :]
""" Kreiere ID für CSV """
id = np.zeros((1,265), dtype=object)
x, y = id.shape
for i in range(x):
    for j in range(y):
        id[i,j] = j
id[0,0] = "id"
header = np.concatenate((header, id), axis=0)
""" Füge alles zusammen """
filtered_pop_grow = np.concatenate((header, body), axis=0)
np.savetxt(FILTERED_POP_GROW_CSV, filtered_pop_grow, delimiter=';', fmt='%s')
# %%
""" Datenveränderung, population_total """
years = np.argwhere(pop_total[1:, 1].astype('float') >= 2005) + 1
body = pop_total[years].reshape(2841, 3)
header = pop_total[:1, :]
""" Kreiere ID für CSV """
id = np.zeros((2842,1), dtype=object)
x, y = id.shape
for i in range(x):
    for j in range(y):
        id[i,j] = i
id[0,0] = "id"
header = np.concatenate((id[:1], header), axis=1)
body = np.concatenate((id[1:], body), axis=1)
""" Füge alles zusammen """
filtered_pop_total = np.concatenate((header, body), axis=0)
np.savetxt(FILTERED_POP_TOTAL_CSV, filtered_pop_total, delimiter=';', fmt='%s')

# %%
""" Datenveränderung, population_gdp """
gdp = gdp.T
greater2005 = np.argwhere(gdp[4:, 0].astype('float') >= 2005) + 4
lower2017 = np.argwhere(gdp[4:, 0].astype('float') <= 2017) + 4 
years = np.intersect1d(greater2005, lower2017)
body = gdp[years]
header = gdp[:4, :]
""" Kreiere ID für CSV """
id = np.zeros((1,265), dtype=object)
x, y = id.shape
for i in range(x):
    for j in range(y):
        id[i,j] = j
id[0,0] = "id"
header = np.concatenate((header, id), axis=0)
""" Füge alles zusammen """
filtered_gdp = np.concatenate((header, body), axis=0)
np.savetxt(FILTERED_GDP_CSV, filtered_gdp, delimiter=';', fmt='%s')

# %%
""" Datenveränderung, co2_emission """
years = np.argwhere(emission[1:, 2].astype('float') >= 2005) + 1
body = emission[years].reshape(2945,4)
header = emission[:1, :]
""" nan's finden und entfernen"""
idx_nan = isnan(body[:, 1])
body_without_nan = np.delete(body, idx_nan, axis=0)
""" Kreiere ID für CSV """
id = np.zeros((body_without_nan.shape[0]+1,1), dtype=object)
x, y = id.shape
for i in range(x):
    for j in range(y):
        id[i,j] = i
id[0,0] = "id"
header = np.concatenate((id[:1], header), axis=1)
body = np.concatenate((id[1:], body_without_nan), axis=1)
# """ Füge alles zusammen """
filtered_emission = np.concatenate((header, body), axis=0)
np.savetxt(FILTERED_EMISSION_CSV, filtered_emission, delimiter=';', fmt='%s')

#%%
""" Datenveränderung, air """
greater2005 = np.argwhere(air[1:, 2].astype('float') >= 2005) + 1
lower2017 = np.argwhere(air[1:, 2].astype('float') <= 2017) + 1
years = np.intersect1d(greater2005, lower2017)
body = air[years].reshape(3432,4)
header = air[:1, :]
""" Kreiere ID für CSV """
id = np.zeros((3433,1), dtype=object)
x, y = id.shape
for i in range(x):
    for j in range(y):
        id[i,j] = i
id[0,0] = "id"
header = np.concatenate((id[:1], header), axis=1)
body = np.concatenate((id[1:], body), axis=1)
# """ Füge alles zusammen """
filtered_air = np.concatenate((header, body), axis=0)
np.savetxt(FILTERED_AIR_CSV, filtered_air, delimiter=';', fmt='%s')


# %%

# %%

# %%
